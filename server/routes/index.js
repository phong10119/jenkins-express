var express = require("express");
var router = express.Router();
var users = require("./users");
var blogs = require("./blogs");

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

router.use("/users", users);
router.use("/blogs", blogs);

module.exports = router;
