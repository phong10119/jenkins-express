const Blog = require("../model/blogs");

var express = require("express");
var router = express.Router();

router.get("/", async (req, res) => {
  const blogs = await Blog.find();

  res.json(blogs);
});

router.post("/", async (req, res) => {
  const { slug, content } = req.body;

  if (!slug || !content) {
    return res.status(400).send("Missing slug, or content");
  }

  const blog = new Blog({
    slug,
    content,
  });

  try {
    await blog.save();
    res.send(blog);
  } catch (err) {
    res.status(400).send(err.message);
  }
});

module.exports = router;
