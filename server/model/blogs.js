//Require Mongoose
var mongoose = require("mongoose");

//Define a schema
var Schema = mongoose.Schema;

var BlogSchema = new Schema({
  slug: String,
  content: String,
});

var Blog = mongoose.model("Blog", BlogSchema);

module.exports = Blog;
